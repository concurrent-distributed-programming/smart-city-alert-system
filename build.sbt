name := "ex2"
scalaVersion := "3.3.0"

lazy val AkkaVersion = "2.8.5"

fork := true
fork / run := true

resolvers += "Akka library repository".at("https://repo.akka.io/maven")

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-persistence-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-serialization-jackson" % AkkaVersion,
  "com.typesafe.akka" %% "akka-cluster-sharding-typed" % AkkaVersion,
  "ch.qos.logback" % "logback-classic" % "1.4.7",
  "org.scala-lang.modules" %% "scala-swing" % "3.0.0",
  "com.github.scopt" %% "scopt" % "4.1.0",
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % AkkaVersion % Test,
  "org.scalatest" %% "scalatest" % "3.2.15" % Test
)

assembly / assemblyMergeStrategy := {
  case x if x.endsWith("module-info.class") => MergeStrategy.concat
  case x =>
    val oldStrategy = (assembly / assemblyMergeStrategy).value
    oldStrategy(x)
}

assembly / assemblyJarName := "ex2.jar"
