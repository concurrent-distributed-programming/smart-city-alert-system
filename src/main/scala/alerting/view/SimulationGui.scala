package alerting.view

import alerting.actors.dashboard.Dashboard
import alerting.model.{FireStationState, ZoneState}

import java.awt.Color
import scala.swing.*
import scala.swing.Swing.pair2Dimension

case class SimulationGui(view: Dashboard):

  private lazy val WIDTH = 1024
  private lazy val HEIGHT = 768
  private lazy val quadPanel = new CityOverviewPanel(view)
  private lazy val idToRectangle = view.quad.getAllBounds
  
  private lazy val top: MainFrame = new MainFrame() {
    title = "Smart City Rain Alert System Dashboard"
    size = (WIDTH, HEIGHT)
    resizable = false
    contents = new BoxPanel(Orientation.Vertical) {
      contents += quadPanel
      focusable = true
      listenTo(mouse.clicks, mouse.moves, keys)
      border = Swing.MatteBorder(12, 12, 12, 12, new Color(191, 191, 191))
    }
  }

  def start(): Unit =
    Swing.onEDT {
      top.pack()
      top.centerOnScreen()
      top.open()
    }

  def setAllZonesState(zones: Map[Int, ZoneState.ZoneState]): Unit =
    Swing.onEDT {
      quadPanel.updateAllZonesState(zones)
    }

  def setAllFireStationsState(fireStationsState: Map[Int, FireStationState.FireStationState]): Unit =
    Swing.onEDT {
      quadPanel.updateAllFireStationsState(fireStationsState)
    }

  def enableZone(zoneId: Int): Unit =
    Swing.onEDT {
      quadPanel.enableZone(idToRectangle(zoneId))
    }
      
  def disableZone(zoneId: Int): Unit =
    Swing.onEDT {
      quadPanel.disableZone(idToRectangle(zoneId))
    }
