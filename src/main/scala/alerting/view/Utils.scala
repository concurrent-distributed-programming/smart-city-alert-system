package alerting.view

import alerting.model.SimModel
import alerting.model.SimModel.{Point, Rectangle}

import java.awt.Color
import scala.collection.mutable.ListBuffer
import scala.swing.Graphics2D
import scala.swing.event.MouseClicked

object Utils:
  val unknownFireStation = new Color(134, 55, 166)
  val busyFireStation: Color = new Color(0, 0, 0)
  val availableFireStation = new Color(68, 129, 11)

  private val disabledZone = new Color(191, 191, 191)
  val stableZone = new Color(255, 255, 255)
  val unknownZone = new Color(183, 73, 208)
  val managedZone: Color = new Color(248, 224, 134)
  val alertedZone: Color = new Color(213, 162, 162)

  private val fsSize = 15

  def initZones(allRectangles: List[Rectangle],
                emptyRectangles: List[Rectangle]): Map[Rectangle, Color] =
    allRectangles.map(r =>
      if (emptyRectangles.contains(r)) r -> Color.white
      else r -> unknownZone
    ).toMap

  def initFireStations(allRectangles: List[Rectangle],
                       emptyRectangles: List[Rectangle]): Map[Rectangle, Color] =
    allRectangles.map(r =>
      if (emptyRectangles.contains(r)) r -> Color.white
      else r -> unknownFireStation
    ).toMap

  private def drawFireStations(localRectangle: Rectangle,
                               fireStations: Map[Rectangle, Color],
                               g: Graphics2D): Unit =
    val x = localRectangle.getMidPoint.x.toInt
    val y = localRectangle.getMidPoint.y.toInt
    g.setColor(fireStations(localRectangle))
    g.fillRect(x - fsSize, y - fsSize, fsSize, fsSize)
    g.setColor(Color.black)
    g.drawRect(x - fsSize, y - fsSize, fsSize, fsSize)

  def findSmallestEnclosingRectangle(startingRectangle: Rectangle,
                                     zones: Map[Rectangle, Color],
                                     e: MouseClicked): Rectangle =
    var clickedRectangle = startingRectangle
    var perimeterOfClickedRectangle = clickedRectangle.perimeter
    val clickedPoint = SimModel.Point(e.point.x, e.point.y)

    for (r <- zones.keys)
      if (r.contains(clickedPoint))
        perimeterOfClickedRectangle = math.min(r.perimeter.toInt, perimeterOfClickedRectangle)
        if (perimeterOfClickedRectangle == r.perimeter.toInt) clickedRectangle = r

    clickedRectangle

  def updateCityOverview(sensors: Map[Int, Point],
                         allRectangles: List[Rectangle],
                         emptyRectangles: List[Rectangle],
                         zones: Map[Rectangle, Color],
                         disabledZones: ListBuffer[SimModel.Rectangle],
                         fireStations: Map[Rectangle, Color],
                         g: Graphics2D): Unit =
    for (rec <- allRectangles) do
      if (!emptyRectangles.contains(rec))
        if(disabledZones.contains(rec)) g.setColor(disabledZone)
        else g.setColor(zones(rec))
        rec.fillRect(g)
        g.setColor(Color.black)
        rec.drawRect(g)

        drawFireStations(rec, fireStations, g)

    sensors.values.foreach(p => p.drawPoint(g))