package alerting.view

import alerting.actors.dashboard.Dashboard
import alerting.model.FireStationState.*
import alerting.model.ZoneState.*
import alerting.model.{FireStationState, SimModel, ZoneState}
import alerting.view.Utils.*

import java.awt.{Color, Graphics2D, RenderingHints}
import scala.collection.mutable.ListBuffer
import scala.swing.*
import scala.swing.Swing.pair2Dimension
import scala.swing.event.MouseClicked

class CityOverviewPanel(view: Dashboard) extends Panel:
  background = Color.white
  focusable = true
  listenTo(mouse.clicks)
  preferredSize = (1024, 718)

  private val allSensors = view.quad.getAllElements
  private val allRectangles = view.quad.getAllBounds
  private val emptyRectangles = view.quad.getAllBoundsWithoutElements

  private var zones = initZones(allRectangles, emptyRectangles)
  private val disabledZones = ListBuffer.empty[SimModel.Rectangle]
  private var fireStations = initFireStations(allRectangles, emptyRectangles)

  reactions += {
    case e: MouseClicked =>
      val clickedRectangle = findSmallestEnclosingRectangle(view.quad.bounds, zones, e)
      val zoneId = allRectangles.indexOf(clickedRectangle)
      if(!emptyRectangles.contains(clickedRectangle))
        if(disabledZones.contains(clickedRectangle)) view.askEnableRainGauge(zoneId)
        else view.askDisableRainGauge(zoneId)
  }

  def enableZone(r: SimModel.Rectangle): Unit =
    disabledZones -= r
    repaint()

  def disableZone(r: SimModel.Rectangle): Unit =
    disabledZones += r
    repaint()

  def updateAllZonesState(zones: Map[Int, ZoneState.ZoneState]): Unit =
    zones.filter((id, _) => id != -1)
      .foreach((id, zone) => updateZoneState(allRectangles(id), zone))
    repaint()

  def updateAllFireStationsState(fireStations: Map[Int, FireStationState.FireStationState]): Unit =
    fireStations.filter((id, _) => id != -1)
      .foreach((id, state) => updateFireStationState(allRectangles(id), state))
    repaint()

  private def updateZoneState(zone: SimModel.Rectangle, zoneState: ZoneState.ZoneState): Unit =
    zoneState match
      case Alerted => zones += (zone -> alertedZone)
      case Managed => zones += (zone -> managedZone)
      case Stable => zones += (zone -> stableZone)
      case ZoneState.Unknown => zones += (zone -> unknownZone)
      
  private def updateFireStationState(zone: SimModel.Rectangle, fState: FireStationState.FireStationState): Unit =
    fState match
      case Available => fireStations += (zone -> availableFireStation)
      case Busy => fireStations += (zone -> busyFireStation)
      case FireStationState.Unknown => fireStations += (zone -> unknownFireStation)

  override def paintComponent(g: Graphics2D): Unit =
    super.paintComponent(g)
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
    updateCityOverview(
      allSensors,
      allRectangles,
      emptyRectangles,
      zones,
      disabledZones,
      fireStations,
      g
    )
