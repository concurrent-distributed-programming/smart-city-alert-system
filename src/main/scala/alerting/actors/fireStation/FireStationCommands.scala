package alerting.actors.fireStation

import akka.actor.typed.ActorRef
import alerting.actors.CborSerializable
import alerting.actors.fireStation.State.FireStationSummary

object FireStationCommands:
  sealed trait Command extends CborSerializable
  
  final case class ZoneAlerted() extends Command
  final case class ZoneStable() extends Command
  
  final case class SetZone(zoneId: Int) extends Command

  final case class GetFireStationState(replyTo: ActorRef[FireStationSummary]) extends Command
  final case class FireStationShutdown() extends Command
