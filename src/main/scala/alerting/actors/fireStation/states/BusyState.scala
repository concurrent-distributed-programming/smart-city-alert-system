package alerting.actors.fireStation.states

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.scaladsl.Effect
import alerting.actors.fireStation.FireStationCommands.*
import alerting.actors.fireStation.FireStationEvents.{Event, FireStationAvailable}
import alerting.actors.fireStation.State
import alerting.actors.fireStation.Utils.{fireStationManagement, sendMsgToRainGauge}
import alerting.actors.rainGauge.RainGaugeCommands.{ZoneAlertedAck, ZoneStableAck}

object BusyState:
  def busyFireStation(state: State, command: Command, context: ActorContext[Command]): Effect[Event, State] =
    command match
      case ZoneStable() =>
        context.log.info(f"[${state.fireStationState}-${state.zoneId.getOrElse(-1)}]" +
          f" Received ZoneStable, transitioning to Available State")
        Effect.persist(FireStationAvailable()).thenRun(_ =>
          if (state.zoneId.isDefined)
            val rainGaugeId = f"raingauge-${state.zoneId.get}"
            sendMsgToRainGauge(context, rainGaugeId, ZoneStableAck())
        )
      case ZoneAlerted() =>
        context.log.info(f"[${state.fireStationState}-${state.zoneId.getOrElse(-1)}] Received ZoneAlerted")
        Effect.none.thenRun(_ =>
          if (state.zoneId.isDefined)
            val rainGaugeId = f"raingauge-${state.zoneId.get}"
            sendMsgToRainGauge(context, rainGaugeId, ZoneAlertedAck())
        )
      case cmd => fireStationManagement(cmd, context)
