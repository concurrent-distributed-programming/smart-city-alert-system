package alerting.actors.fireStation

import alerting.actors.CborSerializable
import alerting.model.FireStationState.{Available, Busy}

object FireStationEvents:
  sealed trait Event extends CborSerializable
  final case class FireStationBusy() extends Event
  final case class FireStationZoneSet(zoneId: Int) extends Event
  final case class FireStationAvailable() extends Event

  def eventHandler(state: State, event: Event): State =
    event match
      case FireStationBusy() =>state.setState(Busy)
      case FireStationZoneSet(id) => state.setZone(id)
      case FireStationAvailable() => state.setState(Available)