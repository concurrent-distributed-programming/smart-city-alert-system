package alerting.actors.fireStation

import alerting.actors.CborSerializable
import alerting.actors.fireStation.State.FireStationSummary
import alerting.actors.rainGauge.Utils.unknownZoneId
import alerting.model.FireStationState.Available
import alerting.model.{FireStationState, FireStationStateType}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration

object State:
  def apply(fireStationId: String, zoneId: Option[Int] = Option.empty): State =
    State(fireStationId, zoneId, Available)

  final case class FireStationSummary(fireStationId: String,
                                      zoneId: Int,
                                      @JsonScalaEnumeration(classOf[FireStationStateType]) state: FireStationState.FireStationState = Available
                                     ) extends CborSerializable

final case class State(fireStationId: String,
                       zoneId: Option[Int],
                       @JsonScalaEnumeration(classOf[FireStationStateType]) fireStationState: FireStationState.FireStationState
                      ) extends CborSerializable:

  def setState(state: FireStationState.FireStationState): State = copy(fireStationState = state)

  def setZone(id: Int): State = copy(zoneId = Some(id))

  def toSummary: FireStationSummary =
    FireStationSummary(
      fireStationId,
      zoneId.getOrElse(unknownZoneId),
      fireStationState
    )
