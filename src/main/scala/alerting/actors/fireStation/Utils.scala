package alerting.actors.fireStation

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.scaladsl.{Effect, RetentionCriteria, SnapshotCountRetentionCriteria}
import alerting.Utils.getEntityRefFromId
import alerting.actors.fireStation.FireStationCommands.{Command, FireStationShutdown, GetFireStationState, SetZone}
import alerting.actors.fireStation.FireStationEvents.{Event, FireStationZoneSet}
import alerting.actors.rainGauge.{RainGauge, RainGaugeCommands}

object Utils:
  
  val retentionCriteria: SnapshotCountRetentionCriteria =
    RetentionCriteria.snapshotEvery(numberOfEvents = 5, keepNSnapshots = 3)
  
  def sendMsgToRainGauge(context: ActorContext[FireStationCommands.Command],
                         rainGaugeId: String,
                         message: RainGaugeCommands.Command): Unit =
    val rainGaugeRef = getEntityRefFromId[RainGaugeCommands.Command](context, RainGauge.TypeKey, rainGaugeId)
    if (rainGaugeRef.isDefined) rainGaugeRef.get ! message

  def fireStationManagement(command: Command, context: ActorContext[Command]): Effect[Event, State] =
    command match
      case SetZone(zoneId) =>
        context.log.debug("Received SetZone")
        Effect.persist(FireStationZoneSet(zoneId))
      case GetFireStationState(replyTo) =>
        Effect.none.thenReply(replyTo)(state => state.toSummary)
      case FireStationShutdown() =>
        context.log.info("Received FireStationShutdown")
        Effect.stop()
      case other =>
        context.log.info(f"Unknown message $other")
        Effect.none