package alerting.actors.fireStation

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityTypeKey}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.EventSourcedBehavior
import alerting.Utils.{stashCapacity, supervisorStrategy}
import alerting.actors.fireStation.FireStationCommands.{Command, FireStationShutdown}
import alerting.actors.fireStation.FireStationEvents.{Event, eventHandler}
import alerting.actors.fireStation.Utils.retentionCriteria
import alerting.actors.fireStation.states.AvailableState.availableFireStation
import alerting.actors.fireStation.states.BusyState.busyFireStation
import alerting.model.FireStationState.Busy

object FireStation:
  val TypeKey: EntityTypeKey[Command] = EntityTypeKey[Command]("firestation")

  def initSharding(system: ActorSystem[_]): Unit =
    ClusterSharding(system).init(Entity(TypeKey) { entityContext =>
      val fireStationId = entityContext.entityId
      FireStation(PersistenceId(entityContext.entityTypeKey.name, fireStationId))
    }.withStopMessage(FireStationShutdown()))

  def apply(persistenceId: PersistenceId): Behavior[Command] =
     Behaviors.setup { context =>
       val fireStationId = persistenceId.entityId
       context.log.info(f"FireStation $fireStationId started")

       EventSourcedBehavior[Command, Event, State](
         persistenceId,
         State(fireStationId),
         commandHandler = (state, command) => {
           if (state.fireStationState == Busy) busyFireStation(state, command, context)
           else availableFireStation(state, command, context)
         },
         eventHandler
       ).withRetention(retentionCriteria)
         .onPersistFailure(supervisorStrategy)
         .withStashCapacity(stashCapacity)
     }
