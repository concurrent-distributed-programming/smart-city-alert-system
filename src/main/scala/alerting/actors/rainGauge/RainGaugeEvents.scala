package alerting.actors.rainGauge

import alerting.actors.CborSerializable
import alerting.model.ZoneState.{Alerted, Managed, Stable}

object RainGaugeEvents:
  sealed trait Event extends CborSerializable
  
  final case class RainGaugeSetZone(zoneId: Int) extends Event
  final case class RainLevelRecorded(sensorId: String, value: Int) extends Event
  final case class SensorRegistered(sensorId: String) extends Event
  final case class SensorDeregistered(sensorId: String) extends Event
  final case class RainGaugeStable() extends Event
  final case class RainGaugeManaged() extends Event
  final case class RainGaugeAlerted() extends Event
  final case class RainGaugeEnabled() extends Event
  final case class RainGaugeDisabled() extends Event
  
  def eventHandler(state: State, event: Event): State =
    event match
      case RainGaugeSetZone(zoneId) => state.setZone(zoneId)
      case RainLevelRecorded(sensorId, value) => state.recordRainLevel(sensorId, value).checkThreshold()
      case SensorRegistered(sensorId) => state.registerSensor(sensorId)
      case SensorDeregistered(sensorId) => state.deregisterSensor(sensorId).checkThreshold()
      case RainGaugeStable() => state.setState(Stable)
      case RainGaugeManaged() => state.setState(Managed)
      case RainGaugeAlerted() => state.setState(Alerted)
      case RainGaugeEnabled() => state.setEnabled(true)
      case RainGaugeDisabled() => state.setEnabled(false)
      