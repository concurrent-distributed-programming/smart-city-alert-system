package alerting.actors.rainGauge

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityTypeKey}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import alerting.Utils.{stashCapacity, supervisorStrategy}
import alerting.actors.rainGauge.RainGaugeCommands.{CheckZoneState, Command, RainGaugeShutdown}
import alerting.actors.rainGauge.RainGaugeEvents.*
import alerting.actors.rainGauge.Utils.{rainGaugeTimer, retentionCriteria}
import alerting.actors.rainGauge.states.AlertState.alertedRainGauge
import alerting.actors.rainGauge.states.DisabledState.disabledRainGauge
import alerting.actors.rainGauge.states.ManagedState.managedRainGauge
import alerting.actors.rainGauge.states.StableState.stableRainGauge
import alerting.model.ZoneState
import alerting.model.ZoneState.{Alerted, Managed, Stable, Unknown}

object RainGauge:
  val TypeKey: EntityTypeKey[Command] = EntityTypeKey[Command]("raingauge")
  
  def initSharding(system: ActorSystem[_]): Unit =
    ClusterSharding(system).init(Entity(TypeKey) { entityContext =>
      val rainGaugeId = entityContext.entityId
      RainGauge(PersistenceId(entityContext.entityTypeKey.name, rainGaugeId))
    }.withStopMessage(RainGaugeShutdown()))
  
  def apply(persistenceId: PersistenceId): Behavior[Command] =
    Behaviors.setup { context =>
      val rainGaugeId = persistenceId.entityId
      context.log.info("RainGauge {} started", rainGaugeId)
      
      context.scheduleOnce(rainGaugeTimer, context.self, CheckZoneState())
      
      EventSourcedBehavior[Command, Event, State](
        persistenceId,
        State(rainGaugeId),
        commandHandler = (state, command) => {
          if(state.isEnabled)
            state.zoneState match
              case Stable => stableRainGauge(state, command, context)
              case Managed => managedRainGauge(state, command, context)
              case Alerted => alertedRainGauge(state, command, context)
              case Unknown => Effect.none
          else disabledRainGauge(state, command, context)
        },
        eventHandler
      ).withRetention(retentionCriteria)
        .onPersistFailure(supervisorStrategy)
        .withStashCapacity(stashCapacity)
    }
