package alerting.actors.rainGauge

import alerting.actors.CborSerializable
import alerting.actors.rainGauge.State.{RainGaugeSummary, ZoneSummary}
import alerting.actors.rainGauge.Utils.{SensorReadings, TrackedSensors, threshold, unknownZoneId}
import alerting.model.{ZoneState, ZoneStateType}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object State:
  def apply(rainGaugeId: String, zoneId: Option[Int] = Option.empty): State =
    State(
      rainGaugeId,
      zoneId,
      trackedSensors = ListBuffer.empty,
      lastSensorReadings = mutable.Map.empty,
      ZoneState.Stable,
      false,
      true
    )

  final case class RainGaugeSummary(
    rainGaugeId: String,
    zoneId: Int,
    trackedSensors: TrackedSensors = ListBuffer.empty,
    lastSensorReadings: SensorReadings = mutable.Map.empty,
    @JsonScalaEnumeration(classOf[ZoneStateType]) zoneState: ZoneState.ZoneState = ZoneState.Stable,
    isOverThreshold: Boolean = false,
    isEnabled: Boolean = true
  ) extends CborSerializable
  
  final case class ZoneSummary(
    zoneId: Int,
    @JsonScalaEnumeration(classOf[ZoneStateType]) zoneStateEnum: ZoneState.ZoneState
  ) extends CborSerializable

final case class State(
    rainGaugeId: String,
    zoneId: Option[Int],
    trackedSensors: TrackedSensors,
    lastSensorReadings: SensorReadings,
    @JsonScalaEnumeration(classOf[ZoneStateType]) zoneState: ZoneState.ZoneState,
    isOverThreshold: Boolean,
    isEnabled: Boolean
  ) extends CborSerializable:

  def registerSensor(sensorId: String): State =
    copy(rainGaugeId, zoneId, trackedSensors += sensorId)

  def deregisterSensor(sensorId: String): State =
    copy(rainGaugeId, zoneId, trackedSensors -= sensorId, lastSensorReadings -= sensorId)

  def recordRainLevel(sensorId: String, reading: Int): State =
    copy(rainGaugeId, zoneId, trackedSensors, lastSensorReadings += sensorId -> reading)

  def setState(zoneState: ZoneState.ZoneState): State = copy(zoneState = zoneState)
  
  def setZone(id: Int): State = copy(zoneId = Some(id))
  
  def setEnabled(isEnabled: Boolean): State = copy(isEnabled = isEnabled)

  def checkThreshold(): State =
    val quorum = lastSensorReadings.size / 2 + lastSensorReadings.size % 2
    val voters = lastSensorReadings.map((_, v) => v).filter(_ > threshold).size
    if (voters >= quorum && voters > 0) copy(isOverThreshold = true)
    else copy(isOverThreshold = false)

  def toSummary: RainGaugeSummary =
    RainGaugeSummary(
      rainGaugeId, 
      zoneId.getOrElse(unknownZoneId), 
      trackedSensors, 
      lastSensorReadings, 
      zoneState,
      isOverThreshold
    )
    
  def toZoneSummary: ZoneSummary = ZoneSummary(zoneId.getOrElse(unknownZoneId), zoneState)