package alerting.actors.rainGauge

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.scaladsl.{Effect, RetentionCriteria, SnapshotCountRetentionCriteria}
import alerting.Utils.getEntityRefFromId
import alerting.actors.Sensor.SensorRegistrationConfirmed
import alerting.actors.dashboard.Dashboard.RainGaugeDisableAck
import alerting.actors.fireStation.FireStationCommands.Command
import alerting.actors.fireStation.{FireStation, FireStationCommands}
import alerting.actors.rainGauge.RainGaugeCommands.*
import alerting.actors.rainGauge.RainGaugeEvents.*

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.{DurationInt, FiniteDuration}

object Utils:
  type TrackedSensors = ListBuffer[String]
  type SensorReadings = mutable.Map[String, Int]

  val threshold = 35
  val unknownZoneId: Int = -1
  val rainGaugeTimer: FiniteDuration = 5.second

  val retentionCriteria: SnapshotCountRetentionCriteria =
    RetentionCriteria.snapshotEvery(numberOfEvents = 50, keepNSnapshots = 3)

  def sendMsgToFireStation(context: ActorContext[RainGaugeCommands.Command],
                           fireStationId: String,
                           message: FireStationCommands.Command): Unit =
    val fireStationRef = getEntityRefFromId[FireStationCommands.Command](context, FireStation.TypeKey, fireStationId)
    if (fireStationRef.isDefined) fireStationRef.get ! message

  def rainGaugeManagement(state: State,
                          command: RainGaugeCommands.Command,
                          context: ActorContext[RainGaugeCommands.Command]): Effect[Event, State] =
    command match
      case SetZone(zoneId) =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received SetZone")
        Effect.persist(RainGaugeSetZone(zoneId))
      case RecordRainLevel(sensorId, value) =>
        context.log.debug(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Recording from {} value {}", sensorId, value)
        if (state.trackedSensors.contains(sensorId)) Effect.persist(RainLevelRecorded(sensorId, value))
        else Effect.unhandled
      case SensorRegistrationRequest(sensorId, sensorActor) =>
        context.log.debug(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Registration request from {}", sensorId)
        Effect.persist(SensorRegistered(sensorId)).thenRun(_ =>
            sensorActor ! SensorRegistrationConfirmed()
            context.watchWith(sensorActor, SensorTermination(sensorId))
        )
      case SensorTermination(sensorId) =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Termination notification from {}", sensorId)
        Effect.persist(SensorDeregistered(sensorId))
      case GetRainGaugeState(replyTo) =>
        context.log.debug(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received GetRainGaugeState")
        Effect.none.thenReply(replyTo)(state => state.toSummary)
      case GetZoneState(replyTo) =>
        context.log.debug(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received GetZoneState")
        Effect.none.thenReply(replyTo)(state => state.toZoneSummary)
      case EnableRainGauge(_) =>
        context.log.debug(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received EnableRainGauge")
        Effect.unhandled
      case DisableRainGauge(replyTo) =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received DisableRainGauge")
        if(state.zoneId.isDefined)
          Effect.persist(RainGaugeDisabled()).thenReply(replyTo)(state => RainGaugeDisableAck(state.zoneId.get))
        else Effect.none.thenRun(_ => context.log.info("Ignoring request since zone id is not initialized"))
      case RainGaugeShutdown() =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received RainGaugeShutdown")
        Effect.stop()
      case other =>
        context.log.info(f"Unknown message $other")
        Effect.none
