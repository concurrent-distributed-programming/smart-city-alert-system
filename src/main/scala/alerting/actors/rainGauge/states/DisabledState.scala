package alerting.actors.rainGauge.states

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.scaladsl.Effect
import alerting.actors.dashboard.Dashboard.RainGaugeEnableAck
import alerting.actors.rainGauge.RainGaugeCommands.*
import alerting.actors.rainGauge.RainGaugeEvents.{Event, RainGaugeEnabled}
import alerting.actors.rainGauge.Utils.rainGaugeTimer
import alerting.actors.rainGauge.{RainGaugeCommands, State}

object DisabledState:
  def disabledRainGauge(state: State,
                        command: Command,
                        context: ActorContext[RainGaugeCommands.Command]): Effect[Event, State] =
    command match
      case SetZone(_) => Effect.stash()
      case SensorRegistrationRequest(_, _) => Effect.stash()
      case RecordRainLevel(_, _) => Effect.stash()
      case SensorTermination(_) => Effect.stash()

      case ZoneStableAck() | ZoneAlertedAck() => Effect.stash()

      case GetRainGaugeState(replyTo) => Effect.none.thenReply(replyTo)(state => state.toSummary)
      case GetZoneState(replyTo) => Effect.none.thenReply(replyTo)(state => state.toZoneSummary)
      case CheckZoneState() => Effect.none
      
      case EnableRainGauge(replyTo) =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received EnableRainGauge")
        if (state.zoneId.isDefined)
          replyTo ! RainGaugeEnableAck(state.zoneId.get)
          context.scheduleOnce(rainGaugeTimer, context.self, CheckZoneState())
          Effect.persist(RainGaugeEnabled()).thenUnstashAll()
        else Effect.none.thenRun(_ => context.log.info("Ignoring request since zone id is not initialized"))
      case DisableRainGauge(_) => Effect.unhandled
      case RainGaugeShutdown() => Effect.stop()
