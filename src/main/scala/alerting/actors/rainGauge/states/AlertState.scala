package alerting.actors.rainGauge.states

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.scaladsl.Effect
import alerting.actors.fireStation.FireStationCommands.ZoneAlerted
import alerting.actors.rainGauge.RainGaugeCommands.*
import alerting.actors.rainGauge.RainGaugeEvents.*
import alerting.actors.rainGauge.Utils.{rainGaugeManagement, rainGaugeTimer, sendMsgToFireStation}
import alerting.actors.rainGauge.{RainGaugeCommands, State}

object AlertState:
  def alertedRainGauge(state: State,
                       command: RainGaugeCommands.Command, 
                       context: ActorContext[RainGaugeCommands.Command]): Effect[Event, State] =
    command match
      case ZoneStableAck() => 
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received ZoneStableAck, ignoring")
        Effect.none
      case ZoneAlertedAck() =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received ZoneAlertedAck, transitioning to the Managed State")
        Effect.persist(RainGaugeManaged()).thenUnstashAll()
      case CheckZoneState() =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Trying transition to Managed State")
        context.scheduleOnce(rainGaugeTimer, context.self, CheckZoneState())
        Effect.none.thenRun(state =>
          val msg = ZoneAlerted()
          if (state.zoneId.isDefined)
            val fireStationId = f"firestation-${state.zoneId.get}"  
            sendMsgToFireStation(context, fireStationId, msg)
        )
      case cmd => rainGaugeManagement(state, cmd, context)
        