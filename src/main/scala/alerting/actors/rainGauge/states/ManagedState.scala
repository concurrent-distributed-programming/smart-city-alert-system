package alerting.actors.rainGauge.states

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.scaladsl.Effect
import alerting.actors.fireStation.FireStationCommands.ZoneStable
import alerting.actors.rainGauge.RainGaugeCommands.*
import alerting.actors.rainGauge.RainGaugeEvents.*
import alerting.actors.rainGauge.Utils.{rainGaugeManagement, rainGaugeTimer, sendMsgToFireStation}
import alerting.actors.rainGauge.{RainGaugeCommands, State}

object ManagedState:
  def managedRainGauge(state: State,
                       command: RainGaugeCommands.Command,
                       context: ActorContext[RainGaugeCommands.Command]): Effect[Event, State] =
    command match
      case ZoneStableAck() =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received ZoneStableAck, transitioning to the Stable State")
        Effect.persist(RainGaugeStable()).thenUnstashAll()
      case ZoneAlertedAck() => 
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received ZoneAlertedAck, ignoring")
        Effect.none
      case CheckZoneState() =>
        context.scheduleOnce(rainGaugeTimer, context.self, CheckZoneState())
        if (!state.isOverThreshold)
          context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Trying transition to Stable State")
          Effect.none.thenRun(state =>
            val msg = ZoneStable()
            if(state.zoneId.isDefined)
              val fireStationId = f"firestation-${state.zoneId.get}"
              sendMsgToFireStation(context, fireStationId, msg)
          )
        else
          context.log.debug(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Cannot start transition to Stable State")
          Effect.none
      case cmd => rainGaugeManagement(state, cmd, context)
