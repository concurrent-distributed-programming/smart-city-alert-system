package alerting.actors.rainGauge.states

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.scaladsl.Effect
import alerting.actors.rainGauge.RainGaugeCommands.*
import alerting.actors.rainGauge.RainGaugeEvents.*
import alerting.actors.rainGauge.Utils.{rainGaugeManagement, rainGaugeTimer}
import alerting.actors.rainGauge.{RainGaugeCommands, State}

object StableState:
  def stableRainGauge(state: State,
                      command: RainGaugeCommands.Command,
                      context: ActorContext[RainGaugeCommands.Command]): Effect[Event, State] =
    command match
      case ZoneStableAck() =>
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received ZoneStableAck")
        Effect.none
      case ZoneAlertedAck() => 
        context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Received ZoneAlertedAck")
        Effect.none
      case CheckZoneState() =>
        context.scheduleOnce(rainGaugeTimer, context.self, CheckZoneState())
        if (state.isOverThreshold)
          context.log.info(f"[${state.zoneState}-${state.zoneId.getOrElse(-1)}] Transitioning to Alert State")
          Effect.persist(RainGaugeAlerted())
        else Effect.none
      case cmd => rainGaugeManagement(state, cmd, context)