package alerting.actors.rainGauge

import akka.actor.typed.ActorRef
import alerting.actors.dashboard.Dashboard
import alerting.actors.rainGauge.RainGaugeEvents.*
import alerting.actors.rainGauge.State.{RainGaugeSummary, ZoneSummary}
import alerting.actors.{CborSerializable, Sensor}

object RainGaugeCommands:
  sealed trait Command extends CborSerializable
  
  final case class CheckZoneState() extends Command

  final case class SensorRegistrationRequest(sensorId: String, replyTo: ActorRef[Sensor.Command]) extends Command
  final case class RecordRainLevel(sensorId: String, value: Int) extends Command
  final case class SensorTermination(sensorId: String) extends Command
  
  final case class ZoneStableAck() extends Command
  final case class ZoneAlertedAck() extends Command
  
  final case class GetRainGaugeState(replyTo: ActorRef[RainGaugeSummary]) extends Command
  final case class GetZoneState(replyTo: ActorRef[ZoneSummary]) extends Command
  final case class SetZone(zoneId: Int) extends Command

  final case class EnableRainGauge(replyTo: ActorRef[Dashboard.Command]) extends Command
  final case class DisableRainGauge(replyTo: ActorRef[Dashboard.Command]) extends Command
  final case class RainGaugeShutdown() extends Command
