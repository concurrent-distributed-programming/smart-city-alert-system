package alerting.actors

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{Behavior, SupervisorStrategy}
import alerting.Utils.{generateRainLevel, getEntityRefFromId}
import alerting.actors.rainGauge.RainGaugeCommands.{RecordRainLevel, SensorRegistrationRequest}
import alerting.actors.rainGauge.{RainGauge, RainGaugeCommands}
import alerting.model.SimModel.Point

import scala.concurrent.duration.DurationInt

object Sensor:
  sealed trait Command extends CborSerializable
  final case class SensorRegistrationConfirmed() extends Command
  private final case class RainLevelUpdate(value: Int) extends Command
  final case class SensorShutdown() extends Command

  def apply(sensorId: String, zoneId: Int, location: Point): Behavior[Command] =
    Behaviors.supervise(
      Behaviors.setup[Sensor.Command](context => new Sensor(context, sensorId, zoneId, location).initialization())
    ).onFailure[RuntimeException](
      SupervisorStrategy.restart.withLimit(maxNrOfRetries = 3, withinTimeRange = 5.second)
    )

class Sensor(context: ActorContext[Sensor.Command], sensorId: String, zoneId: Int, location: Point):
  import Sensor.*
  context.log.debug("Sensor {} at zone {} and location {} started", sensorId, zoneId, location)
  
  val rainGaugeId = f"raingauge-$zoneId"

  private def initialization(): Behavior[Command] =
    val localRainGaugeRef = getEntityRefFromId[RainGaugeCommands.Command](context, RainGauge.TypeKey, rainGaugeId)
    if(localRainGaugeRef.isDefined) localRainGaugeRef.get ! SensorRegistrationRequest(sensorId, context.self)
    Behaviors.receiveMessagePartial[Command] {
      case SensorRegistrationConfirmed() => active()
      case SensorShutdown() => Behaviors.stopped
    }
  
  private def active(): Behavior[Command] =
    Behaviors.withTimers[Command] { timers =>
      timers.startSingleTimer(RainLevelUpdate(generateRainLevel()), 1.second)
      Behaviors.receiveMessagePartial {
        case RainLevelUpdate(value) =>
          context.log.debug("Recorded rain level reading {}", value)
          val rainGaugeRef = getEntityRefFromId[RainGaugeCommands.Command](context, RainGauge.TypeKey, rainGaugeId)
          if (rainGaugeRef.isDefined) rainGaugeRef.get ! RecordRainLevel(sensorId, value)
          active()
        case SensorShutdown() => Behaviors.stopped
      }
    }
