package alerting.actors.dashboard

import scala.concurrent.duration.FiniteDuration
import concurrent.duration.DurationInt

object Utils:
  val askUpdateFrequency: FiniteDuration = 5.second
  val queryTimeout: FiniteDuration = 3.second
