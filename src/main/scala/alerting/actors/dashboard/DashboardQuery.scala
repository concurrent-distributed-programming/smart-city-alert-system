package alerting.actors.dashboard

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.{ActorContext, TimerScheduler}
import alerting.Utils.getEntityRefFromId
import alerting.actors.CborSerializable
import alerting.actors.dashboard.Dashboard.GetCityOverview
import alerting.actors.fireStation.{FireStation, FireStationCommands}
import alerting.actors.fireStation.FireStationCommands.GetFireStationState
import alerting.actors.rainGauge.RainGaugeCommands.GetRainGaugeState
import alerting.actors.rainGauge.State.RainGaugeSummary
import alerting.actors.fireStation.State.FireStationSummary
import alerting.actors.rainGauge.{RainGauge, RainGaugeCommands, State}
import alerting.model.{FireStationState, ZoneState}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.FiniteDuration

object DashboardQuery:
  sealed trait Command extends CborSerializable
  private case class QueryTimeout() extends Command
  private final case class WrappedGetRainGaugeState(response: RainGaugeSummary) extends Command
  private final case class WrappedGetFireStationState(response: FireStationSummary) extends Command

  def apply(nZones: Int, emptyZones: List[Int], replyTo: ActorRef[Dashboard.GetCityOverview], timeout: FiniteDuration): Behavior[Command] =
    Behaviors.setup(context =>
      Behaviors.withTimers(timer =>
        new DashboardQuery(nZones, emptyZones, replyTo, timeout, context, timer).onMessage()
      )
    )

class DashboardQuery(nZones: Int,
                     emptyZones: List[Int],
                     replyTo: ActorRef[Dashboard.GetCityOverview],
                     timeout: FiniteDuration,
                     context: ActorContext[DashboardQuery.Command],
                     timers: TimerScheduler[DashboardQuery.Command]):
  import DashboardQuery.*

  timers.startSingleTimer(QueryTimeout(), timeout)

  private val rainGaugeStateAdapter = context.messageAdapter(WrappedGetRainGaugeState.apply)
  private val fireStationStateAdapter = context.messageAdapter(WrappedGetFireStationState.apply)

  private val rainGaugesReplies = mutable.Map.empty[Int, ZoneState.ZoneState]
  private val fireStationReplies = mutable.Map.empty[Int, FireStationState.FireStationState]
  
  private var pendingRainGauges = ListBuffer.empty[String]
  private var pendingFireStations = ListBuffer.empty[String]
  
  // query all rain gauges and fire stations
  for id <- 0 until nZones do
    if(!emptyZones.contains(id))
      val rainGaugeId = "raingauge-" + id
      pendingRainGauges += rainGaugeId
      val rainGaugeRef = getEntityRefFromId[RainGaugeCommands.Command](context, RainGauge.TypeKey, rainGaugeId)
      if (rainGaugeRef.isDefined)
        context.log.debug(f"Asking state to $rainGaugeId")
        rainGaugeRef.get ! GetRainGaugeState(rainGaugeStateAdapter)

      val fireStationId = "firestation-" + id
      pendingFireStations += fireStationId
      val fireStationRef = getEntityRefFromId[FireStationCommands.Command](context, FireStation.TypeKey, fireStationId)
      if (fireStationRef.isDefined)
        context.log.debug(f"Asking state to $fireStationId")
        fireStationRef.get ! GetFireStationState(fireStationStateAdapter)
  
  private def onMessage(): Behavior[DashboardQuery.Command] =
    Behaviors.receiveMessagePartial {
      case QueryTimeout() => onQueryTimeout()
      case WrappedGetRainGaugeState(summary) => onRainGaugeStateUpdate(summary)
      case WrappedGetFireStationState(summary) => onFireStationStateUpdate(summary)
    }

  private def setMissingZonesAsUnknown(): Unit =
    val presentZonesRg = rainGaugesReplies.keySet
    val missingZones = mutable.Set.empty[Int]
    for id <- 0 until nZones do
      if (!emptyZones.contains(id) && !presentZonesRg.contains(id)) missingZones += id
    for id <- missingZones do rainGaugesReplies += id -> ZoneState.Unknown

    val presentZonesFs = fireStationReplies.keySet
    val missingZonesFs = mutable.Set.empty[Int]
    for id <- 0 until nZones do
      if (!emptyZones.contains(id) && !presentZonesFs.contains(id)) missingZonesFs += id
    for id <- missingZones do fireStationReplies += id -> FireStationState.Unknown
  
  private def onQueryTimeout(): Behavior[DashboardQuery.Command] =
    setMissingZonesAsUnknown()
    pendingRainGauges = ListBuffer.empty[String]
    pendingFireStations = ListBuffer.empty[String]
    sendResponse()

  private def onRainGaugeStateUpdate(summary: RainGaugeSummary): Behavior[DashboardQuery.Command] =
    val zoneId = summary.zoneId
    rainGaugesReplies += (zoneId -> summary.zoneState)
    pendingRainGauges -= summary.rainGaugeId
    sendResponse()

  private def onFireStationStateUpdate(summary: FireStationSummary): Behavior[DashboardQuery.Command] =
    val zoneId = summary.zoneId
    fireStationReplies += (zoneId -> summary.state)
    pendingFireStations -= summary.fireStationId
    sendResponse()

  private def sendResponse(): Behavior[DashboardQuery.Command] =
    if(pendingRainGauges.isEmpty && pendingFireStations.isEmpty)
      replyTo ! GetCityOverview(fireStationReplies.toMap, rainGaugesReplies.toMap)
      Behaviors.stopped
    else Behaviors.same
