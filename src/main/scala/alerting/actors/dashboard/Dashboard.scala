package alerting.actors.dashboard

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import alerting.Utils.getEntityRefFromId
import alerting.actors.CborSerializable
import alerting.actors.dashboard.Utils.{askUpdateFrequency, queryTimeout}
import alerting.actors.rainGauge.RainGaugeCommands.{DisableRainGauge, EnableRainGauge}
import alerting.actors.rainGauge.{RainGauge, RainGaugeCommands}
import alerting.model.{FireStationState, QuadTree, ZoneState}
import alerting.view.SimulationGui

object Dashboard:
  sealed trait Command extends CborSerializable
  final case class RainGaugeEnableAck(zoneId: Int) extends Command
  final case class RainGaugeDisableAck(zoneId: Int) extends Command
  final case class QueryCityState() extends Command
  final case class GetCityOverview(fireStationsState: Map[Int, FireStationState.FireStationState],
                                   rainGaugesState: Map[Int, ZoneState.ZoneState]) extends Command

  def apply(dashboardId: String,quad: QuadTree[Int], guiEnabled: Boolean = true): Behavior[Command] =
    Behaviors.setup(context =>
      Behaviors.withTimers(
        timers => new Dashboard(dashboardId, quad, quad.getAllBounds.size, context, timers, guiEnabled).active()
      )
    )

case class Dashboard(dashboardId: String,
                     quad: QuadTree[Int],
                     nZones: Int,
                     context: ActorContext[Dashboard.Command],
                     timers: TimerScheduler[Dashboard.Command],
                     guiEnabled: Boolean):
  import Dashboard.*
  context.log.info("Dashboard {} started", dashboardId)

  private val gui = SimulationGui(this)
  private val allRectangles = quad.getAllBounds
  private val emptyZones = quad.getAllBoundsWithoutElements.map(rec => allRectangles.indexOf(rec))
  if(guiEnabled) gui.start()

  timers.startTimerWithFixedDelay(QueryCityState(), askUpdateFrequency)

  private def active(): Behavior[Command] =
    Behaviors.receiveMessagePartial {
      case QueryCityState() =>
        context.spawnAnonymous(DashboardQuery(nZones, emptyZones, context.self, queryTimeout))
        context.log.info(f"Spawned Dashboard Query with #zones = $nZones")
        Behaviors.same
      case GetCityOverview(fireStationsState, rainGaugesState) =>
        if(guiEnabled)
          gui.setAllFireStationsState(fireStationsState)
          gui.setAllZonesState(rainGaugesState)
        Behaviors.same
      case RainGaugeEnableAck(zoneId) =>
        if(guiEnabled) gui.enableZone(zoneId)
        context.log.info("Rain Gauge in zone {} enabled", zoneId)
        Behaviors.same
      case RainGaugeDisableAck(zoneId) =>
        if(guiEnabled) gui.disableZone(zoneId)
        context.log.info("Rain Gauge in zone {} disabled", zoneId)
        Behaviors.same
    }
  
  private def askRainGauge(zoneId: Int, request: ActorRef[Command] => RainGaugeCommands.Command): Unit =
    val rainGaugeId = f"raingauge-$zoneId"
    val localRainGaugeRef = getEntityRefFromId[RainGaugeCommands.Command](context, RainGauge.TypeKey, rainGaugeId)
    if (localRainGaugeRef.isDefined) localRainGaugeRef.get ! request(context.self)

  def askEnableRainGauge(zoneId: Int): Unit = askRainGauge(zoneId, EnableRainGauge.apply)

  def askDisableRainGauge(zoneId: Int): Unit = askRainGauge(zoneId, DisableRainGauge.apply)

