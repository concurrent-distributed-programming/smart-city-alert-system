package alerting

import alerting.model.CLIConfig
import alerting.model.CLIConfig.SimConfig
import scopt.OParser

/*
 * Starts an actor system of the dashboard and an actor system for each zone
 * They all run on the same process, on the same JVM
 * They are part of an Akka Cluster configured to run locally on different ports
 */
@main def main(args: String*): Unit =
  OParser.parse(CLIConfig.argParser, args, SimConfig()) match
    case Some(config) => App(config)
    case _ => sys.exit(1)
    