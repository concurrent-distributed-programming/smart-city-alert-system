package alerting

import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.{BackoffSupervisorStrategy, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, EntityRef, EntityTypeKey}
import alerting.model.SimModel.Point
import alerting.model.{QuadTree, SimModel}

import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

object Utils:
  val clusterSeedPort = 2551 // refer to application.conf
  val max_X = 1024
  val max_Y = 718
  private val seed = 35
  private val rand = new scala.util.Random(seed)
  private val maxRainLevel = 50
  
  val stashCapacity = 100
  val supervisorStrategy: BackoffSupervisorStrategy =
    SupervisorStrategy.restartWithBackoff(200.millis, 5.seconds, 0.1)

  type SensorMap = Map[Int, Point]
  private type ZoneMap = Map[Int, SensorMap]

  def getEntityRefFromId[A](context: ActorContext[_], 
                            typeKey: EntityTypeKey[A], 
                            rainGaugeId: String): Option[EntityRef[A]] =
    val sharding = ClusterSharding(context.system)
    try
      Some(sharding.entityRefFor(typeKey, rainGaugeId))
    catch
      case ex: IllegalStateException =>
        context.log.debug("{}", ex.toString)
        Option.empty
    
  def genRandomField(n_points: Int): Seq[Point] =
    for _ <- 0 until n_points yield Point(30 + rand.nextInt(max_X - 30), 30 + rand.nextInt(max_Y - 30))
  
  def getZoneMap(quad: QuadTree[Int]): ZoneMap =
    val idToPoint = quad.getAllElements
    val idToRectangle = quad.getAllBounds
    quad.getAllElementsWithBounds
      .groupBy((_, y) => y)
      .map((x, y) => x -> y.keys.toList)
      .map((x, y) => idToRectangle.indexOf(x) -> y)
      .map((x, y) => x -> y.map(z => z -> idToPoint(z)).toMap)

  def generateRainLevel(): Int = rand.nextInt(maxRainLevel)
