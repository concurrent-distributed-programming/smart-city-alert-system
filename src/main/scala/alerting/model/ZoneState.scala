package alerting.model

import com.fasterxml.jackson.core.`type`.TypeReference

object ZoneState extends Enumeration {
  type ZoneState = Value
  val Alerted, Stable, Managed, Unknown = Value
}

class ZoneStateType extends TypeReference[ZoneState.type]