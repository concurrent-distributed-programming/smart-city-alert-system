package alerting.model

import com.fasterxml.jackson.core.`type`.TypeReference

object FireStationState extends Enumeration {
  type FireStationState = Value
  val Available, Busy, Unknown = Value
}

class FireStationStateType extends TypeReference[FireStationState.type]
