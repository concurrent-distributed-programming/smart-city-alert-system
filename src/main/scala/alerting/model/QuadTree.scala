package alerting.model

import alerting.model.SimModel.Quadrant.{BOTTOM_LEFT, BOTTOM_RIGHT, TOP_LEFT, TOP_RIGHT}
import alerting.model.SimModel.{Node, Point, Quadrant, Rectangle}

import scala.collection.mutable
import scala.collection.mutable.{ListBuffer, Map}

final case class QuadTree[E](elementPerQuad: Int, level: Int = 1, bounds: Rectangle):
  private val children: mutable.Map[Quadrant, Option[QuadTree[E]]] = mutable.Map(
    TOP_LEFT -> None,
    TOP_RIGHT -> None,
    BOTTOM_LEFT -> None,
    BOTTOM_RIGHT -> None
  )
  private val elements: mutable.ArrayDeque[Node[E]] = mutable.ArrayDeque()

  private val minx = bounds.minx
  private val miny = bounds.miny
  private val maxx = bounds.maxx
  private val maxy = bounds.maxy

  def getAllBounds: List[Rectangle] =
    val rectangleList: ListBuffer[Rectangle] = ListBuffer.empty
    rectangleList += bounds
    for c <- children do
      if (c._2.nonEmpty)
        rectangleList.addAll(c._2.get.getAllBounds)
    rectangleList.toList.sortBy(x => (x.fy, x.sy, x.fx, x.sx))

  def getAllElements: Predef.Map[E, Point] =
    val elementList: mutable.Map[E, Point] = mutable.Map.empty
    elementList.addAll(elements.map(f => f.elem -> Point(f.x, f.y)))
    for c <- children do
      if (c._2.nonEmpty)
        elementList.addAll(c._2.get.getAllElements)
    elementList.toMap

  def getAllElementsWithBounds: Predef.Map[E, Rectangle] =
    val elementList: mutable.Map[E, Rectangle] = mutable.Map.empty
    elementList.addAll(elements.map(f => f.elem -> bounds))
    for c <- children do
      if (c._2.nonEmpty)
        elementList.addAll(c._2.get.getAllElementsWithBounds)
    elementList.toMap

  def getAllBoundsWithoutElements: List[Rectangle] =
    val emptyBounds = ListBuffer.empty[Rectangle]
    if(elements.isEmpty) emptyBounds += bounds
    for c <- children do
      if (c._2.nonEmpty)
        emptyBounds.addAll(c._2.get.getAllBoundsWithoutElements)
    emptyBounds.toList.sortBy(x => (x.fy, x.sy, x.fx, x.sx))
    
  def insert(e: E, p: Point): Unit =
    if (bounds.contains(p))
      selectChild(p.x, p.y) match
        case Some(quad) =>
          quad.insert(e, p)
        case None =>
          elements += new Node[E](e, p.x, p.y)
          if (elements.size > elementPerQuad)
            subdivide()
            for elem <- elements do
              val quad = selectChild(elem.x, elem.y)
              if (quad.nonEmpty) quad.get.insert(elem.elem, Point(elem.x, elem.y))
            elements.clear()

  private def subdivide(): Unit = for (c <- Quadrant.values) do createChildIfAbsent(c)

  private def createChildIfAbsent(c: Quadrant) =
    def maxX(c: Quadrant) = c match {
      case TOP_RIGHT | BOTTOM_RIGHT => maxx
      case BOTTOM_LEFT | TOP_LEFT => bounds.getMidPoint.x
    }

    def maxY(c: Quadrant) = c match {
      case BOTTOM_LEFT | BOTTOM_RIGHT => bounds.getMidPoint.y
      case TOP_RIGHT | TOP_LEFT => maxy
    }

    def minX(c: Quadrant) = c match {
      case TOP_RIGHT | BOTTOM_RIGHT => bounds.getMidPoint.x
      case BOTTOM_LEFT | TOP_LEFT => minx
    }

    def minY(c: Quadrant) = c match {
      case BOTTOM_LEFT | BOTTOM_RIGHT => miny
      case TOP_RIGHT | TOP_LEFT => bounds.getMidPoint.y
    }

    if (children(c).isEmpty)
      children.put(c, Some(create(Rectangle(minX(c), minY(c), maxX(c), maxY(c)))))

  private def create(rectangle: Rectangle) = new QuadTree[E](elementPerQuad, level + 1, rectangle)

  private def selectChild(x: Double, y: Double): Option[QuadTree[E]] =
    val centerY = bounds.getMidPoint.y
    if (x < bounds.getMidPoint.x)
      if (y < centerY) children(Quadrant.BOTTOM_LEFT) else children(Quadrant.TOP_LEFT)
    else if (y < centerY) children(Quadrant.BOTTOM_RIGHT) else children(Quadrant.TOP_RIGHT)

  override def toString: String = bounds.toString + "@" + children.toString()
