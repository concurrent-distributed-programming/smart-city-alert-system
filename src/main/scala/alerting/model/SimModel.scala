package alerting.model

import java.awt.{BasicStroke, Color, Graphics2D}
import scala.math.{abs, max, min}

object SimModel:
  enum Quadrant:
    case TOP_RIGHT, TOP_LEFT, BOTTOM_LEFT, BOTTOM_RIGHT

  case class Node[E](elem: E, x: Double, y: Double):
    override def toString: String = elem.toString + "@[" + x + ", " + y + "]"
  
  case class Point(x: Double, y: Double):
    def drawPoint(g: Graphics2D,
                  size: Int = 8,
                  internalColor: Color = new Color(129, 198, 235),
                  externalColor: Color = new Color(48, 111, 186)): Unit =
      val previousStroke = g.getStroke
      g.setColor(internalColor)
      g.fillOval(x.toInt - (size / 2), y.toInt - (size / 2), size, size)
      val lineStroke = new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND)
      g.setStroke(lineStroke)
      g.setColor(externalColor)
      g.drawOval(x.toInt - (size / 2), y.toInt - (size / 2), size, size)
      g.setStroke(previousStroke)

  case class Rectangle(sx: Double, sy: Double, fx: Double, fy: Double):
    val minx: Double = min(sx, fx)
    val miny: Double = min(sy, fy)
    val maxx: Double = max(sx, fx)
    val maxy: Double = max(sy, fy)
    private val height: Double = abs(fy - sy)
    private val width: Double = abs(fx - sx)

    def getMidPoint: Point = Point(minx + (maxx - minx) / 2, miny + (maxy - miny) / 2)

    def drawRect(g: Graphics2D): Unit = g.drawRect(sx.toInt, sy.toInt, width.toInt, height.toInt)

    def fillRect(g: Graphics2D): Unit = g.fillRect(sx.toInt, sy.toInt, width.toInt, height.toInt)

    def perimeter: Double = height * 2 + width * 2

    def contains(p: Point): Boolean = p.x >= minx && p.y >= miny && p.x < maxx && p.y < maxy
