package alerting.model

import scopt.{OParser, OParserBuilder}

object CLIConfig:
  case class SimConfig(nSensors: Int = 30, guiEnabled: Boolean = true)

  private val MIN_SENSORS = 1
  private val MAX_SENSORS = 30
  
  private val builder: OParserBuilder[SimConfig] = OParser.builder[SimConfig]
  val argParser: OParser[Unit, SimConfig] =
    import builder.*
    OParser.sequence(
      programName("ex2.jar"),
      opt[Int]('s', "nSensors")
        .text("Number of sensors to simulate")
        .action((s, c) => c.copy(nSensors = s))
        .validate(s =>
          if (MIN_SENSORS to MAX_SENSORS contains s) success
          else failure(f"Given nSensors: $s. Must be in [$MIN_SENSORS, $MAX_SENSORS]")
        ),
      opt[Boolean]('g', "guiEnabled")
        .text("Whether to use the GUI")
        .action((g, c) => c.copy(guiEnabled = g)),
      help("help").text("Prints this usage text")
    )
  