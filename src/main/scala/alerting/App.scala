package alerting

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import alerting.Utils.*
import alerting.actors.Sensor
import alerting.actors.dashboard.Dashboard
import alerting.actors.fireStation.{FireStation, FireStationCommands}
import alerting.actors.rainGauge.RainGaugeCommands.SetZone
import alerting.actors.rainGauge.{RainGauge, RainGaugeCommands}
import alerting.model.CLIConfig.SimConfig
import alerting.model.SimModel.{Point, Rectangle}
import alerting.model.{CLIConfig, QuadTree}
import com.typesafe.config.ConfigFactory

object App:
  private val clusterName = "AlertingSystem"
  private val boundary: Rectangle = Rectangle(0, 0, max_X, max_Y)

  def apply(config: SimConfig): Unit =
    val sensorList : List[Point] = genRandomField(config.nSensors).toList
    
    val quad: QuadTree[Int] = QuadTree(3, 1, boundary)
    for (p, idx) <- sensorList.zipWithIndex do quad.insert(idx, p)

    loadDashboardSystem(quad, config.guiEnabled)

    val zoneMap = getZoneMap(quad)
    println(zoneMap.keys)
    val allRectangles = quad.getAllBounds
    val emptyZones = quad.getAllBoundsWithoutElements.map(rec => allRectangles.indexOf(rec))
    println(emptyZones)

    for (zoneId, sensorMap) <- zoneMap do loadZoneSystem(zoneId, sensorMap)

  private def loadZoneSystem(zoneId: Int, sensorMap: SensorMap) =
    ActorSystem(zoneBehaviour(zoneId, sensorMap), clusterName)

  private def loadDashboardSystem(quad: QuadTree[Int], guiEnabled: Boolean) =
    val config = configWithPort()
    val system = ActorSystem(dashboardBehaviour(quad, guiEnabled), clusterName, config)
    FireStation.initSharding(system)
    RainGauge.initSharding(system)
    system

  private def configWithPort() =
    ConfigFactory.parseString(
      s"""
        akka.remote.artery.canonical.port=$clusterSeedPort
        """).withFallback(ConfigFactory.load())

  private def initFireStation(context: ActorContext[_], zoneId: Int): Unit =
    val fireStationId = f"firestation-$zoneId"
    val localFireStationRef = getEntityRefFromId[FireStationCommands.Command](context, FireStation.TypeKey, fireStationId)
    if (localFireStationRef.isDefined) localFireStationRef.get ! FireStationCommands.SetZone(zoneId)

  private def initRainGauge(context: ActorContext[_], zoneId: Int): Unit =
    val rainGaugeId = f"raingauge-$zoneId"
    val localRainGaugeRef = getEntityRefFromId[RainGaugeCommands.Command](context, RainGauge.TypeKey, rainGaugeId)
    if (localRainGaugeRef.isDefined) localRainGaugeRef.get ! SetZone(zoneId)

  private def zoneBehaviour(zoneId: Int, sensorMap: SensorMap) =
    Behaviors.setup { context =>
      RainGauge.initSharding(context.system)
      FireStation.initSharding(context.system)
      
      initFireStation(context, zoneId)
      initRainGauge(context, zoneId)

      for (sensorId, location) <- sensorMap do
        val id = f"sensor-$sensorId-$zoneId"
        context.spawn(Sensor(id, zoneId, location), id)

      Behaviors.empty
    }

  private def dashboardBehaviour(quad: QuadTree[Int], guiEnabled: Boolean) =
    Behaviors.setup { context =>
      val dashboardId = "dashboard"
      context.spawn(Dashboard(dashboardId, quad, guiEnabled), dashboardId)
      Behaviors.empty
    }
