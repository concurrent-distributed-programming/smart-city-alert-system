package alerting.model

import alerting.model.SimModel.{Point, Rectangle}
import org.scalatest.matchers.should.Matchers.should
import org.scalatest.wordspec.AnyWordSpecLike

import scala.collection.mutable

class QuadTreeSpec extends AnyWordSpecLike:

  "QuadTree" must:
    "add elements splitting when the maximum number of elements per quad is reached" in:
      val bounds = Rectangle(0, 0, 50, 50)
      val quadTree = new QuadTree[Int](elementPerQuad = 2, level = 1, bounds)

      quadTree.insert(1, Point(2, 3))
      quadTree.insert(2, Point(22, 43))
      quadTree.insert(3, Point(12, 34))

      quadTree.getAllBounds should ===(
        List(
          Rectangle(0.0, 0.0, 25.0, 25.0),
          Rectangle(25.0, 0.0, 50.0, 25.0),
          Rectangle(0.0, 0.0, 50.0, 50.0),
          Rectangle(0.0, 25.0, 25.0, 50.0),
          Rectangle(25.0, 25.0, 50.0, 50.0)
        )
      )
      quadTree.getAllElements should ===(
        mutable.HashMap(
          1 -> Point(2.0,3.0), 
          2 -> Point(22.0,43.0), 
          3 -> Point(12.0,34.0)
        )
      )
      quadTree.getAllElementsWithBounds should ===(
        mutable.HashMap(
          1 -> Rectangle(0.0,0.0,25.0,25.0), 
          2 -> Rectangle(0.0,25.0,25.0,50.0), 
          3 -> Rectangle(0.0,25.0,25.0,50.0)
        )
      )
      quadTree.getAllBoundsWithoutElements should ===(
        List(
          Rectangle(25.0, 0.0, 50.0, 25.0),
          Rectangle(0.0, 0.0, 50.0, 50.0),
          Rectangle(25.0, 25.0, 50.0, 50.0)
        )
      )
