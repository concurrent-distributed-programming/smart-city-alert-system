package alerting.actors

import akka.pattern
import akka.actor.testkit.typed.scaladsl.{LogCapturing, ManualTime, ScalaTestWithActorTestKit}
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity}
import akka.cluster.typed.{Cluster, Join, Leave}
import akka.pattern.StatusReply
import alerting.actors.ActorsTestHelper.clusterConfig
import org.scalatest.wordspec.AnyWordSpecLike
import alerting.actors.Sensor.{SensorRegistrationConfirmed, SensorShutdown}
import alerting.actors.rainGauge.RainGauge.TypeKey
import alerting.actors.rainGauge.RainGaugeCommands.{Command, SensorRegistrationRequest}
import alerting.model.SimModel.Point

import scala.concurrent.duration.DurationInt

class SensorSpec extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(clusterConfig))
  with AnyWordSpecLike:

  private val dummySensorId = "sensor-1"
  private val dummyZoneId = 1
  private val dummyLocation = Point(1, 1)
  private val manualTime: ManualTime = ManualTime()
  private val cluster = Cluster.get(system)

  override protected def beforeAll(): Unit = cluster.manager ! Join.create(cluster.selfMember.address)
  override protected def afterAll(): Unit = cluster.manager ! Leave(cluster.selfMember.address)

  "The Sensor" must:
    "register to a local rainGauge when started" in:
      val mockedRainGaugeBehaviour = Behaviors.setup[Command] { _ =>
        Behaviors.receiveMessagePartial {
          case SensorRegistrationRequest(_, replyTo) =>
            replyTo ! SensorRegistrationConfirmed()
            Behaviors.same
        }
      }

      ClusterSharding(system).init(Entity(TypeKey) { _ => mockedRainGaugeBehaviour })
      manualTime.timePasses(500.millis) // time for the rain gauge to start

      val sensorProbe = createTestProbe[Sensor.Command]()
      spawn(Behaviors.monitor(sensorProbe.ref, Sensor(dummySensorId, dummyZoneId, dummyLocation)))

      sensorProbe.expectMessageType[SensorRegistrationConfirmed]
    
    "terminate after receiving a passivation message" in:
      val probe = createTestProbe[Sensor.Command]()
      val sensorActor = spawn(Sensor(dummySensorId, dummyZoneId, dummyLocation))

      sensorActor ! SensorShutdown()
      probe.expectTerminated(sensorActor, probe.remainingOrDefault)
