package alerting.actors.dashboard

import alerting.actors.dashboard.Dashboard.{GetCityOverview, QueryCityState, RainGaugeDisableAck, RainGaugeEnableAck}
import alerting.actors.fireStation.{FireStation, FireStationCommands}
import alerting.actors.rainGauge.RainGaugeCommands.{DisableRainGauge, EnableRainGauge}
import alerting.actors.rainGauge.{RainGauge, RainGaugeCommands}
import alerting.model.FireStationState.Available
import alerting.model.ZoneState.Stable

import concurrent.duration.DurationInt

class DashboardSpec extends DashboardSpecTemplate:
  
  "The Dashboard" must:
    
    "query all rain gauges and fire stations periodically" in:
      val probe = createTestProbe[Dashboard.Command]()
      spawnDashboard(probe)

      MockedRainGauge.initSharding(system)
      MockedFireStation.initSharding(system)
      println(dummyNZones)
      for zoneId <- 1 to dummyNZones do
        val rainGaugeId = "raingauge-" + zoneId
        val rainGaugeRef = sharding.entityRefFor(RainGauge.TypeKey, rainGaugeId)
        rainGaugeRef ! RainGaugeCommands.SetZone(zoneId)

        val fireStationId = "firestation-" + zoneId
        val fireStationRef = sharding.entityRefFor(FireStation.TypeKey, fireStationId)
        fireStationRef ! FireStationCommands.SetZone(zoneId)

      manualTime.timePasses(5.second)

      probe.expectMessageType[QueryCityState]

      manualTime.timePasses(3.second)

      probe.expectMessage(
        GetCityOverview(
          Map(3 -> Available, 4 -> Available),
          Map(3 -> Stable, 4 -> Stable)
        )
      )

    "disable or enable a rain gauge on request" in:
      val probe = createTestProbe[Dashboard.Command]()
      spawnDashboard(probe)

      MockedRainGauge.initSharding(system)

      manualTime.timePasses(500.millis)

      val zoneId = 1
      val rainGaugeId = "raingauge-" + zoneId
      val rainGaugeRef = sharding.entityRefFor(RainGauge.TypeKey, rainGaugeId)
      rainGaugeRef ! RainGaugeCommands.SetZone(zoneId)

      rainGaugeRef ! DisableRainGauge(probe.ref)
      probe.expectMessage(RainGaugeDisableAck(zoneId))

      rainGaugeRef ! EnableRainGauge(probe.ref)
      probe.expectMessage(RainGaugeEnableAck(zoneId))
