package alerting.actors.dashboard

import akka.actor.testkit.typed.scaladsl.{ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity}
import akka.cluster.typed.{Cluster, Join, Leave}
import alerting.Utils.{genRandomField, max_X, max_Y}
import alerting.actors.ActorsTestHelper.clusterConfig
import alerting.actors.dashboard.Dashboard.{RainGaugeDisableAck, RainGaugeEnableAck}
import alerting.actors.fireStation.FireStationCommands.GetFireStationState
import alerting.actors.fireStation.State.FireStationSummary
import alerting.actors.fireStation.{FireStation, FireStationCommands}
import alerting.actors.rainGauge.RainGaugeCommands.{DisableRainGauge, EnableRainGauge, GetRainGaugeState}
import alerting.actors.rainGauge.State.RainGaugeSummary
import alerting.actors.rainGauge.Utils.unknownZoneId
import alerting.actors.rainGauge.{RainGauge, RainGaugeCommands}
import alerting.model.QuadTree
import alerting.model.SimModel.{Point, Rectangle}
import org.scalatest.wordspec.AnyWordSpecLike

abstract class DashboardSpecTemplate
  extends ScalaTestWithActorTestKit(ManualTime.config.withFallback(clusterConfig))
    with AnyWordSpecLike:

  override protected def beforeAll(): Unit = cluster.manager ! Join.create(cluster.selfMember.address)
  override protected def afterAll(): Unit = cluster.manager ! Leave(cluster.selfMember.address)

  protected val manualTime: ManualTime = ManualTime()
  protected val cluster: Cluster = Cluster.get(system)
  protected val sharding: ClusterSharding = ClusterSharding(system)
  protected val dummyNSensors = 6
  protected val dummyDashboardId = "dashboard-1"
  protected val dummyNZones: Int = getQuad.getAllBounds.size
  
  private def getQuad =
    val sensorList: List[Point] = genRandomField(dummyNSensors).toList
    val boundary: Rectangle = Rectangle(0, 0, max_X, max_Y)
    val quad: QuadTree[Int] = QuadTree(3, 1, boundary)
    for (p, idx) <- sensorList.zipWithIndex do quad.insert(idx, p)
    quad

  def spawnDashboard(probe: TestProbe[Dashboard.Command]): ActorRef[Dashboard.Command] =
    spawn(
      Behaviors.monitor(probe.ref,
        Dashboard(
          dummyDashboardId,
          getQuad,
          guiEnabled = false
        )
      )
    )
    
  object MockedFireStation:
    def initSharding(system: ActorSystem[_]): Unit =
      ClusterSharding(system).init(Entity(FireStation.TypeKey) { entityContext =>
        MockedFireStation(entityContext.entityId)
      })

    def apply(fireStationId: String): Behavior[FireStationCommands.Command] =
      Behaviors.setup { context =>
        context.log.info(f"Started firestation shard $fireStationId")
        var localZoneId = Option.empty[Int]
        Behaviors.receiveMessagePartial {
          case FireStationCommands.SetZone(zoneId) => localZoneId = Some(zoneId); Behaviors.same
          case GetFireStationState(replyTo) =>
            replyTo ! FireStationSummary(fireStationId, localZoneId.getOrElse(unknownZoneId))
            Behaviors.same
        }
      }

  object MockedRainGauge:
    def initSharding(system: ActorSystem[_]): Unit =
      ClusterSharding(system).init(Entity(RainGauge.TypeKey) { entityContext =>
        MockedRainGauge(entityContext.entityId)
      })

    def apply(rainGaugeId: String): Behavior[RainGaugeCommands.Command] =
      Behaviors.setup { context =>
        context.log.info(f"Started raingauge shard $rainGaugeId")
        var localZoneId = Option.empty[Int]
        Behaviors.receiveMessagePartial {
          case RainGaugeCommands.SetZone(zoneId) => localZoneId = Some(zoneId); Behaviors.same
          case GetRainGaugeState(replyTo) =>
            replyTo ! RainGaugeSummary(rainGaugeId, localZoneId.getOrElse(unknownZoneId))
            Behaviors.same
          case EnableRainGauge(replyTo) =>
            replyTo ! RainGaugeEnableAck(localZoneId.getOrElse(unknownZoneId))
            Behaviors.same
          case DisableRainGauge(replyTo) =>
            replyTo ! RainGaugeDisableAck(localZoneId.getOrElse(unknownZoneId))
            Behaviors.same
        }
      }
