package alerting.actors

import akka.actor.testkit.typed.scaladsl.ManualTime
import com.typesafe.config.{Config, ConfigFactory}

import java.util.UUID

object ActorsTestHelper:
  val clusterConfig: Config = ConfigFactory.parseString(
    s"""
        # overwrite to avoid reusing the snapshot directory already used by the main application and to
        # avoid reusing snapshots of previous tests
        akka.persistence.journal.plugin = "akka.persistence.journal.inmem"
        akka.persistence.snapshot-store.plugin = "akka.persistence.snapshot-store.local"
        akka.persistence.snapshot-store.local.dir = "target/snapshot-${UUID.randomUUID().toString}"

        # don't terminate ActorSystem via CoordinatedShutdown in tests
        akka.coordinated-shutdown.terminate-actor-system = off
        akka.coordinated-shutdown.run-by-actor-system-terminate = off
        akka.coordinated-shutdown.run-by-jvm-shutdown-hook = off
        akka.cluster.run-coordinated-shutdown-when-down = off
        
        # overwrite relevant application.conf parameters
        akka.cluster.seed-nodes = []
      """).withFallback(ConfigFactory.load())

  val defaultConfig: Config = ManualTime.config.withFallback(clusterConfig)
  
  private var rainGaugeCounter = 0
  def newRainGaugeId(): String =
    rainGaugeCounter += 1
    f"raingauge-$rainGaugeCounter"
