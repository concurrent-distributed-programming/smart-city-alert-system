package alerting.actors.rainGauge

import akka.actor.testkit.typed.scaladsl.{ManualTime, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.ShardingEnvelope
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityRef}
import akka.cluster.typed.{Cluster, Join, Leave}
import akka.persistence.typed.PersistenceId
import alerting.actors.ActorsTestHelper.{defaultConfig, newRainGaugeId}
import alerting.actors.Sensor
import alerting.actors.Sensor.SensorRegistrationConfirmed
import alerting.actors.fireStation.{FireStation, FireStationCommands}
import alerting.actors.fireStation.FireStationCommands.{ZoneAlerted, ZoneStable}
import alerting.actors.fireStation.Utils.sendMsgToRainGauge
import alerting.actors.rainGauge.RainGaugeCommands.*
import alerting.actors.rainGauge.State.{RainGaugeSummary, ZoneSummary}
import alerting.actors.rainGauge.Utils.threshold
import org.scalatest.BeforeAndAfterEach
import org.scalatest.wordspec.AnyWordSpecLike

import concurrent.duration.DurationInt

abstract class RainGaugeSpecTemplate extends ScalaTestWithActorTestKit(defaultConfig)
  with AnyWordSpecLike with BeforeAndAfterEach:

  override protected def beforeAll(): Unit =
    cluster.manager ! Join.create(cluster.selfMember.address)
    rainGaugeProbe = Some(spawnRainGauge())
    spawnMockedFireStation()
    manualTime.timePasses(500.millis)

  override def beforeEach(): Unit =
    rainGaugeId = Some(newRainGaugeId())
    rainGaugeRef = Some(sharding.entityRefFor(RainGauge.TypeKey, rainGaugeId.get))
    rainGaugeRef.get ! RainGaugeCommands.SetZone(dummyZoneId)
    rainGaugeProbe.get.expectMessageType[SetZone]
    manualTime.timePasses(500.millis)

  override protected def afterAll(): Unit =
    super.afterAll()
    cluster.manager ! Leave(cluster.selfMember.address)

  protected var rainGaugeProbe = Option.empty[TestProbe[RainGaugeCommands.Command]]
  protected var rainGaugeId = Option.empty[String]
  protected var rainGaugeRef = Option.empty[EntityRef[RainGaugeCommands.Command]]

  protected val dummyZoneId = 2
  protected val dummySensorId = "sensor-1"
  protected val dummyUnderThresholdRainLevel = 2
  protected val dummyRainLevel = 2
  protected val dummyOverThresholdRainLevel: Int = threshold + 1
  protected val manualTime: ManualTime = ManualTime()
  protected val cluster: Cluster = Cluster.get(system)
  protected val sharding: ClusterSharding = ClusterSharding(system)
  
  protected val mockedFireStation: Behavior[FireStationCommands.Command] =
    Behaviors.setup[FireStationCommands.Command] { context =>
      Behaviors.receiveMessagePartial {
        case ZoneAlerted() =>
          context.log.info(f"Received notification from ${rainGaugeId.get}")
          sendMsgToRainGauge(context, rainGaugeId.get, ZoneAlertedAck())
          Behaviors.same
        case ZoneStable() =>
          context.log.info(f"Received request from ${rainGaugeId.get}")
          sendMsgToRainGauge(context, rainGaugeId.get, ZoneStableAck())
          Behaviors.same
      }
    }

  protected def spawnMockedFireStation(): ActorRef[ShardingEnvelope[FireStationCommands.Command]] =
    ClusterSharding(system).init(Entity(FireStation.TypeKey) { _ => mockedFireStation })

  protected def spawnRainGauge(persistenceId: PersistenceId): TestProbe[Command] =
    val probe = createTestProbe[RainGaugeCommands.Command]()
    ClusterSharding(system).init(Entity(RainGauge.TypeKey) { _ => 
      Behaviors.monitor(probe.ref, RainGauge(persistenceId))
    })
    probe

  protected def spawnRainGauge(): TestProbe[Command] =
    val probe = createTestProbe[RainGaugeCommands.Command]()
    ClusterSharding(system).init(Entity(RainGauge.TypeKey) { entityContext =>
      Behaviors.monitor(
        probe.ref,
        RainGauge(PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId))
      )
    })
    probe

  protected def registerSensor(sensorId: String, rainGaugeRef: EntityRef[Command]): Unit =
    val probe = createTestProbe[Sensor.Command]()
    rainGaugeRef ! SensorRegistrationRequest(sensorId, probe.ref)
    probe.expectMessageType[SensorRegistrationConfirmed]
    
  protected def registerSensor(rainGaugeRef: EntityRef[Command]): Unit =
    val probe = createTestProbe[Sensor.Command]()
    rainGaugeRef ! SensorRegistrationRequest(dummySensorId, probe.ref)
    probe.expectMessageType[SensorRegistrationConfirmed]

  protected def checkRainGaugeState(rainGauge: EntityRef[RainGaugeCommands.Command],
                                    expectedSummary: RainGaugeSummary): Unit =
    val probe = createTestProbe[RainGaugeSummary]()
    rainGauge ! GetRainGaugeState(probe.ref)
    probe.expectMessage(expectedSummary)

  protected def checkZoneState(rainGauge: EntityRef[RainGaugeCommands.Command],
                                    expectedSummary: ZoneSummary): Unit =
    val probe = createTestProbe[ZoneSummary]()
    rainGauge ! GetZoneState(probe.ref)
    probe.expectMessage(expectedSummary)
