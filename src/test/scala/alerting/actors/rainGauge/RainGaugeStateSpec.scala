package alerting.actors.rainGauge

import alerting.actors.rainGauge.RainGaugeCommands.{GetRainGaugeState, RainGaugeShutdown, RecordRainLevel, SensorRegistrationRequest, SetZone}
import alerting.actors.rainGauge.State.RainGaugeSummary
import concurrent.duration.DurationInt
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class RainGaugeStateSpec extends RainGaugeSpecTemplate:

  "The Rain Gauge" must:
    
    "keep track of registered sensors" in:
      val probe = rainGaugeProbe.get
      val ref = rainGaugeRef.get
      val id = rainGaugeId.get

      registerSensor(ref)
      probe.expectMessageType[SensorRegistrationRequest]

      val summary = RainGaugeSummary(id, dummyZoneId, trackedSensors = ListBuffer(dummySensorId))
      checkRainGaugeState(ref, summary)
      probe.expectMessageType[GetRainGaugeState]

      ref ! RainGaugeShutdown()
      probe.expectMessageType[RainGaugeShutdown]

    "only accept rain level updates from registered sensors" in:
      val probe = rainGaugeProbe.get
      val ref = rainGaugeRef.get
      val id = rainGaugeId.get

      // The rain level is not recorded, the message should be unhandled
      ref ! RecordRainLevel(dummySensorId, dummyUnderThresholdRainLevel)
      probe.expectMessageType[RecordRainLevel]

      checkRainGaugeState(ref, RainGaugeSummary(id, dummyZoneId))
      probe.expectMessageType[GetRainGaugeState]

      // The rain level is recorded
      registerSensor(ref)
      probe.expectMessageType[SensorRegistrationRequest]

      ref ! RecordRainLevel(dummySensorId, dummyUnderThresholdRainLevel)
      probe.expectMessageType[RecordRainLevel]

      checkRainGaugeState(ref, RainGaugeSummary(
        id,
        dummyZoneId,
        trackedSensors = ListBuffer(dummySensorId),
        lastSensorReadings = mutable.Map(dummySensorId -> dummyRainLevel)
      ))
      probe.expectMessageType[GetRainGaugeState]

      ref ! RainGaugeShutdown()
      probe.expectMessageType[RainGaugeShutdown]

    "keep its state" in:
      val probe = rainGaugeProbe.get
      val ref = rainGaugeRef.get
      val id = rainGaugeId.get

      ref ! RainGaugeCommands.SetZone(dummyZoneId)
      probe.expectMessageType[SetZone]

      // update the state
      registerSensor(ref)
      probe.expectMessageType[SensorRegistrationRequest]

      ref ! RecordRainLevel(dummySensorId, dummyRainLevel)
      probe.expectMessageType[RecordRainLevel]

      val summary = RainGaugeSummary(
        id,
        dummyZoneId,
        trackedSensors = ListBuffer(dummySensorId),
        lastSensorReadings = mutable.Map(dummySensorId -> dummyRainLevel)
      )
      checkRainGaugeState(ref, summary)
      probe.expectMessageType[GetRainGaugeState]

      ref ! RainGaugeShutdown()
      probe.expectMessageType[RainGaugeShutdown]

      // start again with same id
      manualTime.timePasses(500.millis)
      ref ! RainGaugeCommands.SetZone(dummyZoneId)
      probe.expectMessageType[SetZone]

      checkRainGaugeState(ref, summary)
      probe.expectMessageType[GetRainGaugeState]

      ref ! RainGaugeShutdown()
      probe.expectMessageType[RainGaugeShutdown]
