package alerting.actors.rainGauge

import alerting.actors.rainGauge.RainGaugeCommands.*
import alerting.actors.rainGauge.State.ZoneSummary
import alerting.actors.rainGauge.Utils.rainGaugeTimer
import alerting.model.ZoneState
import alerting.model.ZoneState.{Alerted, Managed, Stable}

class RainGaugeAlertSpec extends RainGaugeSpecTemplate:

  "The Rain Gauge" must:

    "go in alert state if values over the threshold are recorded" in:
      val probe = rainGaugeProbe.get
      val ref = rainGaugeRef.get

      registerSensor(ref)
      probe.expectMessageType[SensorRegistrationRequest]

      ref ! RecordRainLevel(dummySensorId, dummyOverThresholdRainLevel)
      probe.expectMessageType[RecordRainLevel]

      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]

      checkZoneState(ref, ZoneSummary(dummyZoneId, Alerted))
      probe.expectMessageType[GetZoneState]

      ref ! RainGaugeShutdown()
      probe.expectMessageType[RainGaugeShutdown]

    "go from stable to managed and from managed to stable state only after the intervention of the fire station" in:
      val probe = rainGaugeProbe.get
      val ref = rainGaugeRef.get

      registerSensor(ref)
      probe.expectMessageType[SensorRegistrationRequest]

      ref ! RecordRainLevel(dummySensorId, dummyOverThresholdRainLevel)
      probe.expectMessageType[RecordRainLevel]

      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]

      ref ! RecordRainLevel(dummySensorId, dummyUnderThresholdRainLevel)
      probe.expectMessageType[RecordRainLevel]

      checkZoneState(ref, ZoneSummary(dummyZoneId, Alerted))
      probe.expectMessageType[GetZoneState]

      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]
      probe.expectMessageType[ZoneAlertedAck]

      checkZoneState(ref, ZoneSummary(dummyZoneId, Managed))
      probe.expectMessageType[GetZoneState]

      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]
      probe.expectMessageType[ZoneStableAck]

      checkZoneState(ref, ZoneSummary(dummyZoneId, Stable))
      probe.expectMessageType[GetZoneState]

      ref ! RainGaugeShutdown()
      probe.expectMessageType[RainGaugeShutdown]

    "go to alert state only if the majority of the sensors record values over the threshold" in:
      val probe = rainGaugeProbe.get
      val ref = rainGaugeRef.get

      // register three sensors to the rain gauge
      for id <- 0 to 2 do
        val mockedSensorId = f"sensor-$id"
        registerSensor(mockedSensorId, ref)
        probe.expectMessageType[SensorRegistrationRequest]

      // two of the three record values under the threshold
      ref ! RecordRainLevel("sensor-0", dummyUnderThresholdRainLevel)
      ref ! RecordRainLevel("sensor-1", dummyUnderThresholdRainLevel)
      ref ! RecordRainLevel("sensor-2", dummyOverThresholdRainLevel)

      for _ <- 0 to 2 do probe.expectMessageType[RecordRainLevel]

      // the state of the rain gauge is still expected to be Stable
      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]

      checkZoneState(ref, ZoneSummary(dummyZoneId, Stable))
      probe.expectMessageType[GetZoneState]

      // in case of parity the rain gauge goes to the alert state
      ref ! SensorTermination("sensor-1")
      probe.expectMessageType[SensorTermination]

      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]

      checkZoneState(ref, ZoneSummary(dummyZoneId, Alerted))
      probe.expectMessageType[GetZoneState]

      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]
      probe.expectMessageType[ZoneAlertedAck]

      checkZoneState(ref, ZoneSummary(dummyZoneId, Managed))
      probe.expectMessageType[GetZoneState]

      // if the values are still over the threshold after the fire station
      // intervention no transition happens
      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]

      // if a sensor joins with new data under the threshold the transition happens
      registerSensor("sensor-3", ref)
      probe.expectMessageType[SensorRegistrationRequest]

      ref ! RecordRainLevel("sensor-3", dummyUnderThresholdRainLevel)
      probe.expectMessageType[RecordRainLevel]

      manualTime.timePasses(rainGaugeTimer)
      probe.expectMessageType[CheckZoneState]
      probe.expectMessageType[ZoneStableAck]

      checkZoneState(ref, ZoneSummary(dummyZoneId, Stable))
      probe.expectMessageType[GetZoneState]
      
      ref ! RainGaugeShutdown()
      probe.expectMessageType[RainGaugeShutdown]
